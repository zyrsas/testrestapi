from rest_framework import generics
from rest_framework.decorators import api_view
from tests.serializers import  TestSerializer
from tests.models import Question, Answer, Test, UserInfo
from rest_framework.response import Response


class TestsList(generics.ListCreateAPIView):
    queryset = Test.objects.all()
    serializer_class = TestSerializer


class TestDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Test.objects.all()
    serializer_class = TestSerializer


@api_view(['POST'])
def AnswerCheck(request):
    try:
        obj = Question.objects.filter(question=request.data['question'])
        id_answ = dict(list(obj.values('answer_id'))[0])
        id_answ = int(id_answ['answer_id'])
        objAnswer = Answer.objects.filter(id=id_answ)
        d = dict(list(objAnswer.values('is_correct'))[0])

        if int(request.data['answer']) == int(d['is_correct']):
            flag = "True"
        else:
            flag = "False"
        return Response({"AnswerIsCorrect": flag})
    except ValueError:
        return Response({"AnswerIsCorrect": "Error"})


@api_view(['POST'])
def SaveRezult(request):
    user = UserInfo(name=request.data["user"],
                    rezult=request.data["rezult"],
                    title_test=request.data["title_test"]
                    )
    user.save()
    return Response({"successful": "true"})



